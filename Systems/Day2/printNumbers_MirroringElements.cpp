#include "Header.h"

void printReverse(int number)
{
	if (number == 0)
	{
		printf("0\n");
		return;
	}
	char arr[] = { '0','1','2','E','h','5','9','L','8','6' };
	char *result = (char *)malloc(sizeof(char) * 10);
	int *temp = (int *)malloc(sizeof(int) * 10);
	int ind1 = 0,ind=0;
	while (number > 0)
	{
		temp[ind1++] = number % 10;
		number = number / 10;
	}
	for (int i = ind1 - 1; i >= 0; i--)
	{
		result[ind++] = arr[temp[i]];
	}
	result[ind] = '\0';
	printf("%s\n", result);
}
void printNumbers(int n, int number)
{
	if (n == 1)
	{
		printReverse(number);
		return;
	}
	for (int i = 0; i < 10; i++)
	{
		printNumbers1(n - 1, number * 10 + i);
	}
	return;
}
int main()
{
	int n;
	scanf("%d", &n);
	printNumbers(n, 0);
	return 0;
}