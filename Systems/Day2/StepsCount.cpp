#include "Header.h"

long noOfWays(int n)
{
	if (n == 0)
		return 1;
	long sum = 0;
	for (int i = 1; i <=n; i++)
	{
		sum += noOfWays(n - i);
	}
	return sum;
}
int main()
{
	int n;
	printf("Number of Steps:");
	scanf("%d", &n);
	printf("%ld", noOfWays(n));
	return 0;
}