#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>

struct node
{
	int data;
	struct node *next;
};

typedef struct node Node;
Node * SLReverse(Node *head,Node *pre,Node *curr,Node *nex)
{
	if (curr != NULL)
	{
		curr->next = pre;
		if (nex!=NULL)
		{
			return SLReverse(head,curr,nex,nex->next);
		}
		return SLReverse(head, curr, nex, nex);
	}
	return pre;
}
void display5(struct node *head)
{
	if (head != NULL)
	{
		while (head->next != NULL)
		{
			printf("%d->", head->data);
			head = head->next;
		}
		printf("%d", head->data);
	}
	else
	{
		printf("Empty Linked List");
	}
}
struct node * creation5()
{
	int val;
	printf("Number of Nodes to be Created:");
	scanf("%d", &val);
	struct node *new1, *temp;
	struct node *root = (struct node*)malloc(sizeof(struct node));
	root = NULL;
	while (val > 0)
	{
		val--;
		new1 = (struct node*)malloc(sizeof(struct node));
		scanf("%d", &new1->data);
		temp = root;
		if (temp == NULL)
		{
			root = new1;
			root->next = NULL;
		}
		else
		{
			while (temp->next != NULL)
			{
				temp = temp->next;
			}
			new1->next = NULL;
			temp->next = new1;
		}
	}
	return root;
}
int main()
{
	struct node * head = creation5();
	head = SLReverse(head, NULL, head, head->next);;
	display5(head);
	return 0;
}