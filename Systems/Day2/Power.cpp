#include "Header.h"

unsigned power(int base, int pow)
{
	if (pow == 0)
		return 1;
	else if (pow == 1)
		return base;
	else if (pow & 1)
		return (((power(base, pow / 2) % 1000000007)*(power(base, pow / 2) % 1000000007) % 1000000007)*((base % 1000000007)) % 1000000007);
	return ((power(base, pow / 2) % 1000000007)*(power(base, pow / 2) % 1000000007) % 1000000007);
}
int main(){
	int base,pow;
	printf("Base:");
	scanf("%d", &base);
	printf("Pow:");
	scanf("%d", &pow);
	printf("%u",power(base, pow));
	return 0;
}