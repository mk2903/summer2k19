#include "Header.h"

void printNumbers(int n,int number)
{
	if (n == 1)
	{
		printf("%d\n ", number);
		return;
	}
	for (int i = 0; i < 10; i++)
	{
		printNumbers(n - 1, number * 10 + i);
	}
	return;
}
int main()
{
	int n;
	scanf("%d", &n);
	printNumbers(n,0);
	return 0;
}