#include "Header.h"

int findSum(int arr[], int N)
{
	if (N <= 0)
		return 0;
	return  arr[N - 1]+findSum(arr, N - 1);
}

int main()
{
	int arr[5] = { 1,2,3,4,5 };
	printf("%ld",findSum(arr, 5));
	return 0;
}