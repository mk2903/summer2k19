#include "Header.h"

int fibonacci(int *arr,int n)
{
	if (n == 0 || n == 1)
	{
		return n;
	}
	if (arr[n] != -1)
	{
		return arr[n];
	}
	arr[n] = fibonacci(arr,n - 1) + fibonacci(arr,n - 2);
	return arr[n];
}

int main()
{
	int n;
	scanf("%d",&n);
	int *arr=(int *)malloc(sizeof(int)*(n+1));
	for (int i = 0; i <= n; i++)
	{
		arr[i] = -1;
	}
	printf("%d",fibonacci(arr,n));
	return 0;
}