#include "Header.h"
int OccurencesOfWord(char **matrix,const char *s,int len,int start,int end,int row,int col,int count)
{
	if (start<row && end<col)
	{
		//Loop1-Right
		int i = start,j,k=0,pathj=end;
		for (j = end; j < col && k!=len; j++)
		{
			if (s[k] != matrix[i][j])
				break;
			k++;
		}
		if (k == len)
		{
			printf("%d,%d->%d,%d\n",i,pathj,i,--j);
			count++;
		}
		//Loop2-Left
		i = start, j, k = 0;
		for (j = end; j >=0 && k != len; j--)
		{
			if (s[k] != matrix[i][j])
				break;
			k++;
		}
		if (k == len)
		{
			printf("%d,%d->%d,%d\n", i, pathj, i, ++j);
			count++;
		}
		// Loop3-Down
		int pathi = start;
		i, j=end, k = 0;
		for (i = start; i < row && k != len; i++)
		{
			if (s[k] != matrix[i][j])
				break;
			k++;
		}
		if (k == len)
		{
			printf("%d,%d->%d,%d\n", pathi,j,--i,j);
			count++;
		}
		//Loop4-Up
		i, j = end, k = 0;
		for (i = start; i >= 0 && k != len; i--)
		{
			if (s[k] != matrix[i][j])
				break;
			k++;
		}
		if (k == len)
		{
			printf("%d,%d->%d,%d\n", pathi, j, ++i, j);
			count++;
		}
		// Loop5-RD
		i = start, j=end, k = 0;
		pathi = start; pathj = end;
		for (; i<row && j < col && k != len;i++, j++)
		{
			if (s[k] != matrix[i][j])
				break;
			k++;
		}
		if (k == len)
		{
			printf("%d,%d->%d,%d\n", pathi, pathj, --i, --j);
			count++;
		}
		//Loop6-UL
		i = start, j = end, k = 0;
		for (; i>=0 && j >= 0 && k != len;i--,j--)
		{
			if (s[k] != matrix[i][j])
				break;
			k++;
		}
		if (k == len)
		{
			printf("%d,%d->%d,%d\n", pathi, pathj, ++i, ++j);
			count++;
		}
		// Loop7-UR
		i = start, j, k = 0;
		for (j = end; i>=0 && j < col && k != len;i--,j++)
		{
			if (s[k] != matrix[i][j])
				break;
			k++;
		}
		if (k == len)
		{
			printf("%d,%d->%d,%d\n", pathi, pathj, ++i, --j);
			count++;
		}
		//Loop8-LD
		i = start, j = end, k = 0;
		for (; i < row && j >= 0 && k != len;i++,j--)
		{
			if (s[k] != matrix[i][j])
				break;
			k++;
		}
		if (k == len)
		{
			printf("%d,%d->%d,%d\n", pathi, pathj, --i, ++j);
			count++;
		}
		return OccurencesOfWord(matrix, s, len, ((start*col) + end + 1) / col, ((start*col) + end + 1) % col, row, col, count);
	}
	return count;
}
int main()
{
	int m,n;
	scanf("%d", &m);
	scanf("%d", &n);
	char **matrix = (char **)malloc(sizeof(char *) *m);
	for (int i = 0; i < m; i++)
	{
		matrix[i] = (char *)malloc(sizeof(char) * n);
	}
	char c;
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
		{
			scanf("%c", &matrix[i][j]);
		}
		scanf("%c", &c);
	}
	const char *s = "ab";
	int count=OccurencesOfWord(matrix,s, 2, 0, 0, m, n, 0);
	printf("\nNo. Of Occurences of Given String :%d", count);
	return 0;
}