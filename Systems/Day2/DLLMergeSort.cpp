#include "Header.h"

struct node
{
	int data;
	struct node *next;
	struct node *prev;
};

typedef struct node Node;

void displayPrev(struct node *head)
{
	if (head != NULL)
	{
		while (head->prev != NULL)
		{
			printf("%d->", head->data);
			head = head->prev;
		}
		printf("%d\n", head->data);
	}
	else
	{
		printf("Empty Linked List");
	}
}
void display(struct node *head)
{
	if (head != NULL)
	{
		while (head->next != NULL)
		{
			printf("%d->", head->data);
			head = head->next;
		}
		printf("%d\n", head->data);
	}
	else
	{
		printf("Empty Linked List");
	}
}

Node* merge(Node *head1,Node *head2)
{	
	if (head1 == NULL)
		return head2;
	if (head2 == NULL)
		return head1;
	Node *res_head = NULL,*temp = res_head;
	while (head1 && head2)
	{
		if (head1->data < head2->data)
		{
			if (res_head == NULL)
			{
				res_head = head1;
				res_head->prev = NULL;
				temp = res_head;
			}
			else
			{
				head1->prev = temp;
				temp->next = head1;
				temp = temp->next;
			}
			head1 = head1->next;
		}
		else
		{
			if (res_head == NULL)
			{
				res_head = head2;
				res_head->prev = NULL;
				temp = res_head;
			}
			else
			{
				head2->prev = temp;
				temp->next = head2;
				temp = temp->next;
			}
			head2 = head2->next;
		}
	}
	while (head1!= NULL)
	{
		temp->next = head1;
		head1->prev = temp;
		temp = temp->next;
		head1 = head1->next;
	}
	while (head2!= NULL)
	{
		temp->next = head2;
		head2->prev = temp;
		temp = temp->next;
		head2 = head2->next;
	}
	return res_head;
}
Node * mergeSort(Node *head)
{
	if (head == NULL || head->next == NULL)
		return head;
	Node *slowp = head, *fastp = head->next;
	while (fastp!=NULL && fastp->next != NULL)
	{
		slowp = slowp->next;
		fastp = fastp->next->next;
	}
	Node *head2=slowp->next;
	slowp->next = NULL;
	head2->prev = NULL;
	head=mergeSort(head);
	head2=mergeSort(head2);

	head=merge(head, head2);
	return head;
}

struct node * creation()
{
	int val;
	printf("Number of Nodes to be Created:");
	scanf("%d", &val);
	struct node *newNode, *temp;
	struct node *root = (struct node*)malloc(sizeof(struct node));
	root = NULL;
	while (val > 0)
	{
		val--;
		newNode = (struct node*)malloc(sizeof(struct node));
		scanf("%d", &newNode->data);
		temp = root;
		if (temp == NULL)
		{
			root = newNode;
			root->next = NULL;
			root->prev = NULL;
		}
		else
		{
			while (temp->next != NULL)
			{
				temp = temp->next;
			}
			newNode->prev = temp;
			newNode->next = NULL;
			temp->next = newNode;
		}
	}
	return root;
}

int main()
{
	struct node * head = creation();
	head = mergeSort(head);
	display(head);
	while (head->next != NULL)
	{
		head = head->next;
	}
	displayPrev(head);
	return 0;
}
