#include "Header.h"
struct cell
{
	char *val;
	char *expression;
};

typedef struct cell Cell;

int str_to_number(char left[10])
{
	int number = 0, i = 0;
	while (left[i] != '\0')
	{
		number = number * 10 + left[i] - '0';
		i++;
	}
	return number;
}

int compare(char command[100], char *str, int len)
{
	for (int i = 0; command[i] != '\0'; i++)
	{
		if (command[i] >= 'A' && command[i] <= 'Z')
			command[i] = command[i] + 32;
	}
	int i = 0;
	for (; str[i] != '\0' && command[i] != '\0'; i++)
	{
		if (command[i] != str[i])
			break;
	}
	if (command[i] == '\0' && i == len)
		return 1;
	return 0;
}

void average(char *arg,Cell **sheet)
{
	int row=0, col ;
	for (col = 1; col < 10; col++)
	{
		if (compare(arg, sheet[row][col].val, strlen(sheet[row][col].val)))
		{
			break;
		}
	}
	int avg = 0;
	for (row = 1; row < 10; row++)
	{
		avg += str_to_number(sheet[row][col].val);
	}
	printf("%d\n", avg/9);
}

int tot(int row,Cell **sheet)
{
	int tot = 0;
	for (int col=1; col < 10; col++)
	{
		tot += str_to_number(sheet[row][col].val);
	}
	return tot;
}

void total(char *arg, Cell **sheet)
{
	int col = 0, row;
	for (row = 1; row < 10; row++)
	{
		if (compare(arg, sheet[row][col].val, strlen(sheet[row][col].val)))
		{
			break;
		}
	}
	
	printf("%d\n", tot(row,sheet));
}

void removeSpaces(char *str)
{
	int count = 0;
	for (int i = 0; str[i]; i++) {
		if (str[i] != ' ' && str[i] != 9)
			str[count++] = str[i];
	}
	str[count] = '\0';
}

void topper(char *arg, Cell **sheet)
{
	removeSpaces(arg);
	if (arg[0] == '\0')
	{
		int row=1,top=0,tRow=0,total;
		for (;row < 10; row++)
		{
			total = tot(row,sheet);
			if (total > top)
			{
				top = total;
				tRow = row;
			}
		}
		const char c = '%';
		printf("%s %lf%c\n", sheet[tRow][0].val,((double)top)/9,c);
	}
	else
	{
		int row = 0, col;
		for (col = 1; col < 10; col++)
		{
			if (compare(arg, sheet[row][col].val, strlen(sheet[row][col].val)))
			{
				break;
			}
		}
		row = 1;int top = 0, tRow = 0, sub = 0;
		for (; row < 10; row++)
		{
			sub = str_to_number(sheet[row][col].val);
			if (sub > top)
			{
				top = sub;
				tRow = row;
			}
		}
		printf("%s %d\n", sheet[tRow][0].val,top);
	}
}

void print(Cell **emptysheet)
{
	emptysheet[0][0].val =(char *)"SName";
	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			if (i == 0 || j==0)
				printf("%10s  |", emptysheet[i][j].val);
			else
				printf("%10d  |", str_to_number(emptysheet[i][j].val));
		}
		printf("\n");
		for (int i = 0; i < 15; i++)
		{
			printf("%6c%c%c%c", 45, 45, 45,45);
		}
		printf("\n");
	}
}

int importValidation(char *arg)
{
	if (arg[0] != '\0')
	{
		int i = 1;
		while (arg[i] != 32 && arg[i] != '\0')
		{
			i++;
		}
		if (arg[i] == '\0')
			return 1;
		else
		{
			while (arg[i] != 32 && arg[i] != '\0')
			{
				i++;
			}
			if (arg[i] == '\0')
				return 1;
			return 0;
		}
	}
	return 0;
}

Cell ** creation()
{
	Cell **start = (Cell **)malloc(sizeof(Cell *) * 11);
	for (int i = 0; i < 11; i++)
		start[i] = (Cell *)malloc(sizeof(Cell) * 11);
	for (int i = 0; i < 11; i++)
	{
		for (int j = 0; j < 11; j++)
		{
			start[i][j].val = NULL;
			start[i][j].expression = NULL;
		}
	}
	return start;
}

void import(Cell **temp_excel, char *argument)
{
	char *updatedfilename = (char*)malloc(sizeof(char) * 1000);
	int length = strlen(argument);
	const char *extend = ".csv";
	if (!(argument[length - 1] == extend[3] && argument[length - 2] == extend[2] && argument[length - 3] == extend[1] && argument[length - 4] == extend[0]))
		strcat(argument, extend);
	FILE *fstream;
	fopen_s(&fstream, argument, "r");
	if (fstream == NULL)
	{
		printf("FILE NOT FOUND\n");
	}
	char *str = (char *)malloc(sizeof(char) * 100);
	char *exp = (char *)malloc(sizeof(char) * 80);
	int ind, i, k = 0,row=0,col;
	while (fgets(str, 100, fstream) != NULL)
	{
		ind = 0; col = 0;
		for (int i = 0; str[i] != '\n'; i++)
		{
			if (str[i] == ',')
			{
				exp[ind] = '\0';
				temp_excel[row][col].val = (char *)malloc(sizeof(char) * 20);
				ind = 0;
				for (; exp[ind]; ind++)
					temp_excel[row][col].val[ind] = exp[ind];
				temp_excel[row][col].val[ind] = '\0';
				ind = 0;
				col++;
				continue;
			}
			exp[ind++] = str[i];
		}
		exp[ind] = '\0';
		temp_excel[row][col].val = (char *)malloc(sizeof(char) * 20);
		ind = 0;
		for (; exp[ind]; ind++)
			temp_excel[row][col].val[ind] = exp[ind];
		temp_excel[row][col].val[ind] = '\0';
		row++;
	}
	fclose(fstream);
}

int main()
{
	char input[100];
	Cell **emptysheet = creation();
	while (1)
	{
		printf(">");
		gets_s(input);
		while (input[0] == '\0')
		{
			printf("\n>");
			gets_s(input);
		}
		int i = 0, k = 0;
		char *command = (char *)malloc(sizeof(char) * 100);
		char *arg = (char *)malloc(sizeof(char) * 100);

		if (input[i] != '\0')
		{
			while (input[i] == 32 || input[i] == 9)
			{
				i++;
			}
			while (input[i] != 32 && input[i] != '\0' && input[i] != 9)
			{
				command[k++] = input[i++];
			}
			command[k] = '\0';

			int k = 0;
			arg[k] = '\0';
			for (int j = i; input[i] != '\0';)
			{
				while (input[j] == 32 || input[j] == 9)
				{
					j++;
				}
				while (input[j] != '\0')
				{
					arg[k++] = input[j++];
				}
				arg[k] = '\0';
				break;
			}
		}
		if (compare(command, (char *)"print", 5))
			print(emptysheet);
		else if (compare(command, (char *)"import", 6))
		{
			if (!importValidation(arg))
				printf("Invalid Format\n");
			else
				import(emptysheet, arg);
		}
		else if (compare(command, (char *)"avg", 3))
			average(arg, emptysheet);
		else if (compare(command, (char *)"total", 5))
			total(arg, emptysheet);
		else if (compare(command, (char *)"topper", 6))
			topper(arg, emptysheet);
		else if (compare(command, (char *)"exit", 4))
			break;
		else if (compare(command, (char *)"clear", 5))
			system("cls");
		else
			printf("Invalid Command\n");
	}
	return 0;
}