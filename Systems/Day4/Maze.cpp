#include "Header.h"

int solveMaze(int **maze,int starti,int startj,int endi,int endj,int row,int col,int **sol)
{
	if (starti == endi && startj == endj)
	{
		sol[starti][startj] = 1;
		return 1;
	}
	if ((startj+1 < col) && maze[starti][startj + 1] == 1)
	{
		if (sol[starti][startj + 1] == 0)
		{
			sol[starti][startj + 1] = 1;
			return solveMaze(maze, starti, startj + 1, endi, endj, row, col, sol);
		}
	}
	if ((starti+1 < row) && maze[starti + 1][startj] == 1)
	{
		if (sol[starti+1][startj] == 0)
		{
			sol[starti + 1][startj] = 1;
			return solveMaze(maze, starti + 1, startj, endi, endj, row, col, sol);
		}
	}
	if ((startj-1>= 0) && maze[starti][startj - 1] == 1)
	{
		if (sol[starti][startj - 1] == 0)
		{
			sol[starti][startj - 1] = 1;
			return solveMaze(maze, starti, startj - 1, endi, endj, row, col, sol);
		}
	}
	if ((starti-1 >= 0) && maze[starti - 1][startj] == 1)
	{
		if (sol[starti-1][startj] == 0)
		{
			sol[starti - 1][startj] = 1;
			return solveMaze(maze, starti - 1, startj, endi, endj, row, col, sol);
		}
	}
	return 0;
}
/*int main()
{
	int m, n;
	printf("Rows:");
	scanf("%d", &m);
	printf("Columns:");
	scanf("%d", &n);
	int **maze = (int **)malloc(sizeof(int *) *m);
	for (int i = 0; i < m; i++)
	{
		maze[i] = (int *)malloc(sizeof(int) * n);
	}
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
		{
			scanf("%d", &maze[i][j]);
		}
	}
	int **sol = (int **)malloc(sizeof(int *) *m);
	for (int i = 0; i < m; i++)
	{
		sol[i] = (int *)malloc(sizeof(int) * n);
	}
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
		{
			sol[i][j] = 0;
		}
	}
	int starti, startj, endi, endj;
	printf("starti:");
	scanf("%d", &starti);
	printf("Startj:");
	scanf("%d", &startj);
	printf("Endi:");
	scanf("%d", &endi);
	printf("Endj:");
	scanf("%d", &endj);
	if (solveMaze(maze, starti,startj,endi,endj, m, n, sol))
	{
		printf("Cheese Found\n");
		for (int i = 0; i < m; i++)
		{
			for (int j = 0; j < n; j++)
			{
				printf("%d ", sol[i][j]);
			}
			printf("\n");
		}
	}
	else
		printf("Cheese Not Found");
	return 0;
}*/