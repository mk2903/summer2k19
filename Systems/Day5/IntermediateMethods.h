#ifndef _IntermediateMethiods_h
#define _IntermediateMethiods_h

#define _CRT_SECURE_NO_WARNINGS

#include "structures.h"
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h>

#define TotalDays (30)
#define TimeSlot (15)
#define MaxEvents (20)

static char StartingDay[8];
static char WorkingDays[7][8];
static int NumberOfWorkingDays;
static int WorkingHours[2];
static int NumberOfSlots;

void trim(char *Name,char ch)
{
	int  i = 0, ind = 0;
	char *name = (char *)malloc(sizeof(char) * 20);
	strcpy(name, Name);

	while (name[i] == ch)
	{
		i++;
	}

	for (int j = i; name[j] != '\0'; j++)
	{
		Name[ind++] = name[j];
	}

	Name[ind] = '\0';
}

int compare(char command[20], const char *str, int len)
{
	for (int i = 0; command[i] != '\0'; i++)
	{
		if (command[i] >= 'A' && command[i] <= 'Z')
			command[i] = command[i] + 32;
	}
	int i = 0;
	for (; str[i] != '\0' && command[i] != '\0'; i++)
	{
		if (command[i] != str[i])
			break;
	}
	if (command[i] == '\0' && i == len)
		return 1;
	return 0;
}

void parseCommand(char *command, char **arg)
{
	int j, ind = 0, k = 0, i;

	for (i = 0; command[i] == ' '; i++);

	for (j = i; command[j] != ' '; j++)
	{
		arg[ind][k++] = command[j];
	}
	arg[ind][k] = '\0';

	ind++; k = 0;

	for (i = j + 1; command[i] != '\0'; i++)
	{
		arg[ind][k++] = command[i];
	}
	arg[ind][k] = '\0';
}

int str_to_number(char *left)
{
	unsigned int number = 0; int i = 0;
	while (left[i] != '\0')
	{
		number = number * 10 + left[i] - '0';
		i++;
	}
	return number;
}

ppl *CreatePeopleRecord(ppl *people, char *name)
{
	ppl * newPerson = (ppl *)malloc(sizeof(ppl));
	strcpy(newPerson->name,name);
	newPerson->next = NULL;
	newPerson->cal = (Cal **)malloc(sizeof(Cal *)*(TotalDays));
	for (int i = 0; i < TotalDays; i++)
	{
		newPerson->cal[i] = (Cal *)malloc(sizeof(Cal)*(NumberOfSlots));
	}

	for (int i = 0; i < TotalDays; i++)
	{
		for (int j = 0; j < NumberOfSlots; j++)
		{
			newPerson->cal[i][j].event = '\0';
		}
	}

	if (people == NULL)
		people = newPerson;
	else
	{
		ppl *temp = people;

		while (temp->next != NULL)
		{
			temp = temp->next;
		}
		temp->next = newPerson;
	}

	return people;
}

int tokenizer(char *command, char **arg, char ch)
{
	int j, ind = 0, k = 0, i = 0;

	if (command[0] == '[' || command[0] == '{')
		i = 1;

	trim(command, ' ');

	for (j = i; command[j] != '\0'; j++)
	{
		if (command[j] == ch || command[j] == '[' || command[j] == '{')
		{
			arg[ind][k] = '\0';
			ind++; k = 0; 
			if(command[j] == '[' || command[j] == '{')
				i = 2;
		}
		else if (command[j] == ']' || command[j] == '}')
		{
			if(i==2)
				arg[ind][k++] = command[j];
			break;
		}
		else
			arg[ind][k++] = command[j];
	}
	arg[ind][k] = '\0';

	return ind;
}

void getEventName(char *command,char **args)
{
	int i, ind = 0, k = 0;

	for (i = 1; command[i] != '"'; i++)
	{
		args[ind][k++] = command[i];
	}
	args[ind++][k] = '\0';
	k = 0;

	for (int j = i+1; command[j] != '\0'; j++)
	{
		args[ind][k++] = command[j];
	}
	args[ind][k] = '\0';
}

void workingDays(char *WD)
{
	int j = 0, len = strlen(WD), k = 0, ind = 0;
	if ((WD[j] == '{' && WD[len - 1] == '}') || (WD[j] == '[' && WD[len - 1] == ']'))
	{
		for (j++; WD[j] != '\0'; j++)
		{
			if (WD[j] != ',')
				WorkingDays[ind][k++] = WD[j];
			else
			{
				WorkingDays[ind][k] = '\0';
				ind++; k = 0;
			}
		}
		WorkingDays[ind][k] = '\0';
		NumberOfWorkingDays = ind;
	}
	else
		printf("Invalid Format\n");
}

int slots(int start,int end)
{
	int minutes = abs(start-end);
	int arr[2], i = 0;
	while (i < 2)
	{
		arr[i++] = minutes % 10;
		minutes = minutes / 10;
	}
	minutes = minutes * 60 + (arr[1] * 10 + arr[0]);

	return minutes / TimeSlot;
}

void workingHours(char *command)
{
	char **args = (char **)malloc(sizeof(char *) * 10);

	for (int i = 0; i < 10; i++)
	{
		args[i] = (char *)malloc(sizeof(char) * 20);
	}

	tokenizer(command, args, ' ');

	WorkingHours[0] = str_to_number(args[0]);
	WorkingHours[1] = str_to_number(args[1]);
	
	NumberOfSlots = slots(WorkingHours[0],WorkingHours[1]);
}

void startingDay(char *SD)
{
	int i;
	for (i = 0; i < NumberOfWorkingDays; i++)
	{
		if (compare(WorkingDays[i], (SD), strlen(SD)))
		{
			strcpy(StartingDay, SD);
			break;
		}
	}

	if (i == NumberOfWorkingDays)
	{
		printf("%s is not in WorkingDays\n",SD);
	}
}

void INIT()
{
	StartingDay[0] = '\0';
	
	for (int i = 0; i < 7; i++)
	{
		WorkingDays[i][0] = '\0';
	}

	NumberOfSlots = 0;

	NumberOfWorkingDays = 0;

	WorkingHours[0] = 0;
	WorkingHours[1] = 0;
}

#endif // !_IntermediateMethiods_h
