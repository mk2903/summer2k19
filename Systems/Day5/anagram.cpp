#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h> 
#include<string.h> 
#include<stdlib.h>

char * charCount(char *str)
{
	char *count = (char *)malloc(sizeof(char) * 27);
	for (int i = 0; i < 25; i++)
		count[i] = 0;
	int c = 0;
	for (int i = 0; str[i] != '\0'; i++)
	{
		if (str[i] >= 'a' && str[i] <= 'z')
			c = str[i] - 97;
		else if (str[i] >= 'A' && str[i] <= 'Z')
			c = str[i] - 65;
		if(str[i]!='\n')
			count[c]++;
	}
	return count;
}

void NumberOfAnagrams(char * fileName,char *str)
{
	FILE *fp;
	fopen_s(&fp, fileName, "r");
	char *count = (char *)malloc(sizeof(char) * 27);
	count = charCount(str);
	char *s = (char *)malloc(sizeof(char) * 27);
	char *scount = (char *)malloc(sizeof(char) * 27);
	int i = 0;
	while (fgets(s, 30, fp) != NULL)
	{
		scount = charCount(s);
		for (i = 0; i < 27; i++)
		{
			if (count[i] != scount[i])
				break;
		}
		if (i == 27)
			printf("%s",s);
	}
	fclose(fp);
}

/*int main()
{
	NumberOfAnagrams((char *)"words.txt", (char *)"lentsi");
	return 0;
}*/