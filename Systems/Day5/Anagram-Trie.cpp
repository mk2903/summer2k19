#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <stdbool.h> 

int CHAR_TO_INDEX(char c)
{
	if (c >= 'a' && c <= 'z')
		return (int)c - (int)'a';
	return (int)c - (int)'A';
}

struct node
{
	char *word;
	struct node *next;
};

typedef struct node node;

struct TrieNode
{
	struct TrieNode *children[26];
	bool isEndOfWord;
	node *head;
};

void merge(char *arr, int l, int m, int r)
{
	int i, j, k;
	int n1 = m - l + 1;
	int n2 = r - m;

	char *L = (char *)malloc(sizeof(char)*n1);
	char *R = (char *)malloc(sizeof(char)*n1);

	for (i = 0; i < n1; i++)
		L[i] = arr[l + i];
	for (j = 0; j < n2; j++)
		R[j] = arr[m + 1 + j];

	i = 0;
	j = 0;
	k = l;
	while (i < n1 && j < n2)
	{
		if (CHAR_TO_INDEX(L[i]) <= CHAR_TO_INDEX(R[j]))
		{
			arr[k] = L[i];
			i++;
		}
		else
		{
			arr[k] = R[j];
			j++;
		}
		k++;
	}

	while (i < n1)
	{
		arr[k] = L[i];
		i++;
		k++;
	}

	while (j < n2)
	{
		arr[k] = R[j];
		j++;
		k++;
	}
}

void mergeSort(char *arr, int l, int r)
{
	if (l < r)
	{
		int m = l + (r - l) / 2;

		mergeSort(arr, l, m);
		mergeSort(arr, m + 1, r);

		merge(arr, l, m, r);
	}
}

struct TrieNode *getNode(void)
{
	struct TrieNode *pNode = NULL;

	pNode = (struct TrieNode *)malloc(sizeof(struct TrieNode));

	int i;

	pNode->isEndOfWord = false;
	//if(pNode->head==NULL)
		pNode->head = NULL;

	for (i = 0; i < 26; i++)
		pNode->children[i] = NULL;

	return pNode;
}

node * newNode(char *key)
{
	node *newnode = (node *)malloc(sizeof(node));
	newnode->word = (char *)malloc(sizeof(char)*27);
	int k = 0;
	for (int i = 0; key[i] != '\0'; i++)
	{
		newnode->word[k++] = key[i];
	}
	newnode->word[k] = '\0';
	newnode->next = NULL;
	return newnode;
}

void display(node *temp)
{
	while (temp->next != NULL)
	{
		printf("%s\n",temp->word);
		temp = temp->next;
	}
	printf("%s", temp->word);
}

void insert(struct TrieNode *root,char *key,char *str)
{
	int level;
	int length = strlen(key);
	int index;
	node *temp = NULL;
	struct TrieNode *pCrawl = root;

	for (level = 0; level < length; level++)
	{
		if (key[level] >= 'a' && key[level] <= 'z' || key[level] >= 'A' && key[level] <= 'Z')
		{
			index = CHAR_TO_INDEX(key[level]);
			if (!pCrawl->children[index])
				pCrawl->children[index] = getNode();

			pCrawl = pCrawl->children[index];
		}
	}

	pCrawl->isEndOfWord = true;
	if (pCrawl->head==NULL)
	{
		pCrawl->head = newNode(str);
	}
	else
	{
		temp = pCrawl->head;
		while (temp -> next != NULL)
		{
			temp = temp->next;
		}
		temp->next = newNode(str);
	}
}

void search(struct TrieNode *root,char *key)
{
	int level;
	int length = strlen(key);
	int index;
	struct TrieNode *pCrawl = root;

	for (level = 0; level < length; level++)
	{
		index = CHAR_TO_INDEX(key[level]);

		if (pCrawl->children[index] == NULL)
		{
			break;
		}

		pCrawl = pCrawl->children[index];
	}

	if (pCrawl != NULL && pCrawl->isEndOfWord) {

		display(pCrawl->head);
	}

}

/*int main()
{
	struct TrieNode *root = getNode();
	FILE *fp;
	fopen_s(&fp,"words.txt", "r");
	char *s = (char *)malloc(sizeof(char) * 27);
	char str[27];
	int i = 0,k;
	while (fscanf(fp,"%s",s) != -1)
	{
		k = 0;
		for (i = 0; s[i] != '\0';i++)
		{
			if (s[i] >= 'a' && s[i] <= 'z' || s[i] >= 'A' && s[i] <= 'Z')
			{
				str[k++] = s[i];
			}
		}
		str[k] = '\0';
		mergeSort(s, 0, strlen(s) - 1);
		insert(root, s,str);
	}
	fclose(fp);
	scanf("%s", s);
	printf("\n");
	mergeSort(s, 0, strlen(s) - 1);
	search(root,s);
	return 0;
}*/