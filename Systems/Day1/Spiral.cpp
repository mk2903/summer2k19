#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>

void spiral(int **matrix,int m,int n)
{
	int top = 0,bottom=m-1, left = 0,right = n - 1,i,j;
	int *spiral = (int *)malloc(sizeof(int)*(n*m)),k=0;
	while (left <= right && top <= bottom)
	{
		for (i = left; i <= right; i++)
		{
			spiral[k++] = matrix[top][i];
		}
		top++;
		for (i = top; i <= bottom; i++)
		{
			spiral[k++] = matrix[i][right];
		}
		right--;
		for (i = right; i >=left; i--)
		{
			spiral[k++] = matrix[bottom][i];
		}
		bottom--;
		for (i = bottom; i >=top; i--)
		{
			spiral[k++] = matrix[i][left];
		}
		left++;
	}
	for (i = 0; i < k; i++)
	{
		printf("%d ", spiral[i]);
	}
}
int main()
{
	int m, n;
	printf("ROWS:");
	scanf("%d", &m);
	printf("COLUMNS:");
	scanf("%d", &n);
	printf("DATA:");
	int ** matrix = (int **)malloc(sizeof(int)*m);
	for (int i = 0; i < m; i++)
	{
		matrix[i] = (int *)malloc(sizeof(int)*n);
	}
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
		{
			scanf("%d", &matrix[i][j]);
		}
	}
	spiral(matrix, m, n);
	return 0;
}