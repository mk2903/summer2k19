#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>
struct node
{
	int data;
	struct node *next;
};
struct node * creation()
{
	int val;
	printf("Number of Nodes:");
	scanf("%d", &val);
	struct node *new1, *temp;
	struct node *root = (struct node*)malloc(sizeof(struct node));
	root = NULL;
	while (val > 0)
	{
		val--;
		new1 = (struct node*)malloc(sizeof(struct node));
	
		scanf("%d", &new1->data);
		temp = root;
		if (temp == NULL)
		{
			root = new1;
			root->next = NULL;
		}
		else
		{
			while (temp->next != NULL)
			{
				temp = temp->next;
			}
			new1->next = NULL;
			temp->next = new1;
		}
	}
	return root;
}
void loopDetection(struct node *head)
{
	if(head==NULL || head->next==NULL || head->next->next==NULL)
	{
		printf("No Loop");
	}
	else
	{
		struct node *ptr1 = head->next, *ptr2 = head->next->next;
		while (ptr1 != ptr2)
		{
			if (ptr2 == NULL || ptr2->next == NULL)
			{
				printf("No Loop");
				break;
			}
			ptr1 = ptr1->next;
			ptr2 = ptr2->next->next;
		}
		if (ptr1 == ptr2)
		{
			printf("There is Loop");
		}
	}
}

int main()
{
	struct node *head,*temp;
	head = creation();
	temp = head;
	while (temp->next != NULL)
	{
		temp = temp->next;
	}
	temp->next = head->next->next->next;
	loopDetection(head);
	return 0;
}