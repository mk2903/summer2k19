#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>

void transpose(int ** matrix,int m,int n)
{
	int ** transpose=(int **)malloc(sizeof(int)*n);
	for (int i = 0; i < n; i++)
	{
		transpose[i]= (int *)malloc(sizeof(int)*m);
	}
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			transpose[i][j] = matrix[j][i];
		}
	}
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			printf("%d ",transpose[i][j]);
		}
		printf("\n");
	}
}
int main()
{
	int m, n;
	printf("ROWS:");
	scanf("%d", &m);
	printf("COLUMNS:");
	scanf("%d", &n);
	printf("DATA:");
	int ** matrix = (int **)malloc(sizeof(int)*m);
	for (int i = 0; i < m; i++)
	{
		matrix[i] = (int *)malloc(sizeof(int)*n);
	}
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
		{
			scanf("%d",&matrix[i][j]);
		}
	}
	transpose(matrix, m, n);
	return 0;
}