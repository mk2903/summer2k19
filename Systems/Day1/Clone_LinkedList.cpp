#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>
#include <map>

struct node
{
	int data;
	struct node *next;
	struct node *random;
};
typedef struct node Node;
void displayRandomNodes(struct node *head)
{
	while (head->next != NULL)
	{
		if (head->random != NULL)
		{
			printf("%d->", head->random->data);
		}
		head = head->next;
	}
	printf("%d\n", head->random->data);
}

void displayNextNodes(struct node *head)
{
	if (head != NULL)
	{
		while (head->next != NULL)
		{
			printf("%d->", head->data);
			head = head->next;
		}
		printf("%d", head->data);
	}
	else
	{
		printf("Empty Linked List");
	}
}
/*
Method 1 :- Inserting newly created nodes inbetween corresponding existing nodes, 
			Assigning random nodes of old ones to new ones,
			breaking the links between old ones and new ones
*/
struct node * clone(struct node *head)
{
	if (head == NULL)
	{
		return NULL;
	}
	else
	{
		struct node *temp = head,*p,*q;
		while (temp!=NULL)
		{
			p = temp->next;
			struct node *newnode = (struct node *)malloc(1 * sizeof(struct node));
			newnode->data = temp->data;
			temp->next = newnode;
			newnode->next = p;
			temp = p;
		}
		temp = head;
		while (temp!=NULL)
		{
			if(temp->random!=NULL)
			{
				p = temp->random->next;
				q = temp->next;
				q->random = p;
			}
			else
			{
				q = temp->next;
				q->random = NULL;
			}
			temp=temp->next->next;
		}
		head = head->next;
		temp = head;
		while (temp->next!=NULL)
		{
			p=temp->next;
			temp->next = p->next;
			temp = temp->next;
		}
	}
	return head;
}
Node *get_new_node(int val) {
	Node *new_node = (Node *)malloc(sizeof(Node));
	new_node->data = val;
	new_node->next = nullptr;
	new_node->random = nullptr;
	return new_node;
}
/*
Method 2 :- Using Has Map
*/
struct node *clone_ll_hashmap(struct node *head) {
	std::map<struct node *, struct node *> hash_map;
	struct node *result = NULL, *ptr = head, *result_end = NULL;

	while (ptr != NULL) {
		struct node *new_node = get_new_node(ptr->data);

		hash_map.insert({ ptr, new_node });

		if (result == NULL)
			result = new_node;
		else
			result_end->next = new_node;

		result_end = new_node;
		ptr = ptr->next;
	}

	if (result != NULL)
		result_end->next = NULL;

	for (auto &val : hash_map) {
		if (val.first->random) {
			std::map<struct node *, struct node *>::iterator it;
			it = hash_map.find(val.first->random);

			val.second->random = it->second;
		}
	}

	return result;
}
struct node * creation()
{
	struct node **arr = (struct node **)malloc(sizeof(struct node)*6);
	int i;
	for (i = 0; i < 6; i++)
	{
		struct node *newnode = (struct node *)malloc(sizeof(struct node));
		arr[i] = newnode;
		arr[i]->data = i + 1;
	}
	arr[0]->next = arr[1];
	arr[1]->next = arr[2];
	arr[2]->next = arr[3];
	arr[3]->next = arr[4];
	arr[4]->next = arr[5];
	arr[5]->next = NULL;
	arr[0]->random = arr[2];
	arr[1]->random = arr[4];
	arr[2]->random = arr[5];
	arr[3]->random = arr[1];
	arr[4]->random = NULL;
	arr[5]->random = arr[3];
	return arr[0];
}
int main()
{
	struct node *root,*inp;
	root=creation();
	inp=root;
	root=clone(root);
	displayNextNodes(root);
	displayRandomNodes(root);
	inp=clone_ll_hashmap(inp);
	displayNextNodes(inp);
	displayRandomNodes(inp);
	return 0;
}