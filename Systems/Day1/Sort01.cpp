#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>

void sort01UsingCount(int* arr, int n)
{
	int count0 = 0, count1 = 0, count2 = 0;
	for (int i = 0; i < n; i++) {
		if (arr[i] == 1)
			count1++;
	}
	count0 = n - count1;
	for (int i = 0; i < count0; i++)
		arr[i] = 0;

	for (int i = count0; i < (count0 + count1); i++)
		arr[i] = 1;

	for (int i = 0; i < n; i++)
	{
		printf("%d ", arr[i]);
	}
}

void swap(int*a, int*b) {
	int temp;
	temp = *b;
	*b = *a;
	*a = temp;
}

void bubbleSort(int* a, int n)
{
	for (int i = 0; i < n - 1; i++)
	{
		for (int j = 0; j < n - i - 1; j++)
		{
			if (a[j] > a[j + 1])
			{
				swap(&a[j], &a[j + 1]);
			}
		}
	}
	for (int i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}

}
void sortByLeftRightPointers(int *a, int n)
{
	int lptr=0, rptr=n-1;
	while(lptr<rptr)
	{
		while (a[lptr] != 1 && lptr < rptr)
		{
			lptr++;
		}
		while (a[rptr] != 0 && lptr < rptr)
		{
			rptr--;
		}
		if (a[lptr] == 1 && a[rptr] == 0)
		{
			swap(&a[lptr], &a[rptr]);
		}
	}
	for (int i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
}
int main()
{
	int n;
	printf("Size of array:");
	scanf("%d", &n);
	int *a = (int *)malloc(sizeof(int)*n);
	for (int i = 0; i < n; i++)
	{
		scanf("%d ", &a[i]);
	}
	printf("sort01 Using Count:\n");
	sort01UsingCount(a,n);
	printf("\nBubble Sort:\n");
	bubbleSort(a, n);
	printf("\nsort By Left Right Pointers:\n");
	sortByLeftRightPointers(a, n);
	return 0;
}