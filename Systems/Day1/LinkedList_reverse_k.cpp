#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>

struct node
{
	int data;
	struct node *next;
};

struct node * reverse(struct node *head)
{
	struct node *previous, *current, *nextNode;
	previous = NULL;
	current = head;
	nextNode = head->next;
	while (current != NULL)
	{
		current->next = previous;
		previous = current;
		current = nextNode;
		if (current != NULL)
		{
			nextNode = nextNode->next;
		}
	}
	return previous;
}
struct node * LinkedList_reverse_kNodes(struct node *head, int k)
{
	int len = 0;
	struct node *temp = (struct node *)malloc(sizeof(struct node));
	temp = head;
	while (temp != NULL || k==0)
	{
		len++;
		temp = temp->next;
	}
	if (len==0)
	{
		return NULL;
	}
	else if (k > len)
	{
		return head;
	}
	else if (len == k)
	{
		return reverse(head);
	}
	else
	{
		struct node *p=NULL, *q=NULL,*r=NULL;
		temp = head;
		struct node *root = (struct node *)malloc(sizeof(struct node));
		int c = len / k,i=0;
		while (c!=0)
		{
			p = temp;
			int start = 1;
			while (start < k)
			{
				temp = temp->next;
				q = temp->next;
				start++;
			}
			temp->next = NULL;
			temp = reverse(p);
			if (i == 0)
			{
				root = temp;
				i = 1;
			}
			else
			{
				r->next = temp;
			}
			while (temp->next != NULL)
			{
				temp = temp->next;
			}
			r = temp;
			temp->next = q;
			temp = q;
			c--;
		}
		r->next = q;
		return root;
	}
}
void display(struct node *head)
{
	if (head != NULL)
	{
		while (head->next != NULL)
		{
			printf("%d->", head->data);
			head = head->next;
		}
		printf("%d", head->data);
	}
	else
	{
		printf("Empty Linked List");
	}
}
struct node * creation()
{
	int val;
	printf("Number of Nodes to be Created:");
	scanf("%d", &val);
	struct node *new1, *temp;
	struct node *root = (struct node*)malloc(sizeof(struct node));
	root = NULL;
	while (val > 0)
	{
		val--;
		new1 = (struct node*)malloc(sizeof(struct node));
		scanf("%d", &new1->data);
		temp = root;
		if (temp == NULL)
		{
			root = new1;
			root->next = NULL;
		}
		else
		{
			while (temp->next != NULL)
			{
				temp = temp->next;
			}
			new1->next = NULL;
			temp->next = new1;
		}
	}
	return root;
}
int main()
{
	struct node * head = creation();
	int k;
	printf("Reverse Count:");
	scanf("%d", &k);
	head = LinkedList_reverse_kNodes(head,k);
	display(head);
	return 0;
}