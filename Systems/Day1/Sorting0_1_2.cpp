#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>


void sort012UsingCount(int* arr, int n)
{
	int count0 = 0, count1 = 0, count2 = 0;
	for (int i = 0; i < n; i++) {
		if (arr[i] == 0)
			count0++;
		if (arr[i] == 1)
			count1++;
		if (arr[i] == 2)
			count2++;
	}
	
	for (int i = 0; i < count0; i++)
		arr[i] = 0;

	for (int i = count0; i < (count0 + count1); i++)
		arr[i] = 1;

	for (int i = (count0 + count1); i < n; i++)
		arr[i] = 2;
	
	for (int i = 0; i < n; i++)
	{
		printf("%d ", arr[i]);
	}
}

int main()
{
	int n;
	printf("Size of array:");
	scanf("%d", &n);
	int *a = (int *)malloc(sizeof(int)*n);
	for (int i = 0; i < n; i++)
	{
		scanf("%d ", &a[i]);
	}
	sort012UsingCount(a,n);
	return 0;
}