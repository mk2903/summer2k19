#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>

struct node
{
	int data;
	struct node *next;
};

void displayNodes(struct node *head)
{
	if (head != NULL)
	{
		while (head->next != NULL)
		{
			printf("%d->", head->data);
			head = head->next;
		}
		printf("%d\n", head->data);
	}
	else
	{
		printf("Empty Linked List");
	}
}
struct node * creation()
{
	int val;
	printf("Number of Nodes:");
	scanf("%d", &val);
	struct node *new1, *temp;
	struct node *root = (struct node*)malloc(sizeof(struct node));
	root = NULL;
	while (val > 0)
	{
		val--;
		new1 = (struct node*)malloc(sizeof(struct node));
		//printf("data:");
		scanf("%d", &new1->data);
		temp = root;
		if (temp == NULL)
		{
			root = new1;
			root->next = NULL;
		}
		else
		{
			while (temp->next != NULL)
			{
				temp = temp->next;
			}
			new1->next = NULL;
			temp->next = new1;
		}
	}
	return root;
}
int length(struct node * head)
{
	int len = 0;
	struct node *temp;
	temp = head;
	while (temp != NULL)
	{
		len++;
		temp = temp->next;
	}
	return len;
}
struct node * addition(struct node * headM, struct node * headL, int lmax,int lmin)
{
	int i = 0,carry=0;
	struct node * head = NULL;
	struct node ** stack = (struct node **)malloc(sizeof(struct node)*(lmax+1));
	int top = -1;
	while (lmax > 0)
	{
		struct node *newNode = (struct node*)malloc(sizeof(struct node));
		newNode->next = NULL;
		if (lmin == lmax)
		{
			newNode->data = (headL->data+headM->data)%10;
			carry = (headL->data + headM->data) / 10;
			headL = headL->next;
			headM = headM->next;
			lmin--;
		}
		else
		{
			newNode->data = headM->data;
			headM = headM->next;
		}
		lmax--;
		if (i == 0)
		{
			if (carry == 1)
			{
				struct node *newNode1 = (struct node*)malloc(sizeof(struct node));
				newNode1->data = carry;
				newNode1->next = newNode;
				head = newNode1;
				stack[++top] = head;
				stack[++top] = head->next;
			}
			else
			{
				head = newNode;
				stack[++top] = head;
			}
			i = 1;
		}
		else
		{
			struct node *temp = head;
			while (temp->next != NULL)
			{
				temp = temp->next;
			}
			int k = top;
			while(carry == 1)
			{
				if (top != -1)
				{
					int x = stack[top]->data + 1;
					stack[top]->data = (x) % 10;
					carry = (x) / 10;
					top--;
				}
				else
				{
					struct node *newNode1 = (struct node*)malloc(sizeof(struct node));
					newNode1->data = carry;
					newNode1->next = head;
					head = newNode1;
					carry = 0;
				}
			}
			top = k;
			stack[++top] = newNode;
			temp->next = newNode;
		}
	}
	return head;
}
struct node * addTwoNumbersInLL(struct node * head1, struct node * head2)
{
	int len1 = 0, len2 = 0;
	len1 = length(head1);
	len2 = length(head2);

	if (len1 > len2)
	{
		return addition(head1, head2, len1, len2);
	}
	else
	{
		return addition(head2, head1, len2, len1);
	}
		
}
int main()
{
	struct node *head1=creation();
	struct node *head2 = creation();
	displayNodes(head1);
	displayNodes(head2);
	head1 = addTwoNumbersInLL(head1, head2);
	displayNodes(head1);
	return 0;
}