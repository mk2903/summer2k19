#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>

void debug(char *s) {
	int n;
	for (n = 0; s[n] != '\0'; n++);
	int count,k=0;
	char c;
	for (int i = 0; s[i] != '\0' && i<n;)
	{
		c = s[i];
		count = 1;
		for (int j = i+1; s[i] != '\0'; j++)
		{
			if (c == s[j])
			{
				count++;
			}
			else
			{
				s[k++] = c;
				if (count>1 && count < 10)
				{
					s[k++] = count + 48;
				}
				else if(count >9)
				{
					int *store = (int *)malloc(sizeof(int)*(n / 2)),m=0;
					while (count > 0)
					{
						store[m++] = count % 10;
						count = count / 10;
					}
					for (int j = m-1; j>=0; j--)
					{
						s[k++] = store[j] + 48;
					}
				}
				i = j;
				break;
			}

		}
	}
	s[k] = '\0';
	printf("%s", s);
}

int main()
{
	char *s = (char *)malloc(sizeof(char) * 50);
	scanf("%s", s);
	debug(s);
	return 0;
}
/*
//abc
//aaaaaaaaaaaaa
//aaabbaacc
//aaaaaaaaaaaaabcd*/