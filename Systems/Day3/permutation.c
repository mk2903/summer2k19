#include "Header.h"

void print(int *arr, int n)
{
	for (int i = 0; i <= n; i++)
	{
		printf("%d", arr[i]);
	}
	printf("\n");
}
void swap(int *a, int *b)
{
	int temp;
	temp = *a;
	*a = *b;
	*b = temp;
}

void permutate(int arr[], int start, int end)
{
	if (start == end)
	{
		print(arr, end);
	}
	else
	{
		for (int i = start; i <= end; i++)
		{
			swap(arr + start, arr + i);
			permutate(arr, start + 1, end);
			swap(arr + start, arr + i);
		}
	}
}
/*int main()
{
	int n;
	scanf("%d", &n);
	int *arr = (int *)malloc(sizeof(int)*n);
	for (int i = 0; i < n; i++)
	{
		scanf("%d", &arr[i]);
	}
	permutate(arr, 0, n - 1);
	return 0;
}*/