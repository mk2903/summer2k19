#define _CRT_SECURE_NO_WARNINGS
# include <stdio.h> 
# include <string.h> 
#include<conio.h>
#include<stdlib.h>
int compare(char command[100], const char *str, int len)
{
	for (int i = 0; command[i] != '\0'; i++)
	{
		if (command[i] >= 'A' && command[i] <= 'Z')
			command[i] = command[i] + 32;
	}
	int i = 0;
	for (; str[i] != '\0' && command[i] != '\0'; i++)
	{
		if (command[i] != str[i])
			break;
	}
	if (command[i] == '\0' && i == len)
		return 1;
	return 0;
}
void removeSpaces(char *str)
{
	int count = 0;
	for (int i = 0; str[i]; i++) {
		if (str[i] != ' ' && str[i] != 9)
			str[count++] = str[i];
	}
	str[count] = '\0';
}
int validGetArg(char arg[100])
{
	if (arg[0] >= 'a' && arg[0] <= 'j' || arg[0] >= 'A' && arg[0] <= 'J')
	{
		if (arg[1] >= 48 && arg[1] <= 57)
		{
			int row = arg[1] - 48;
			if (arg[2] >= 48 && arg[2] <= 57 || (arg[2] == '\0') || (arg[2] == 32))
			{
				if (arg[2] >= 48 && arg[2] <= 57 || (arg[2] == 32))
				{
					if (arg[2] == 32)
					{
						int i = 3;
						while (arg[i] == 32) { i++; }
						if (arg[i] == '\0')
							return 1;
						return 0;
					}
					row = row * 10 + arg[2] - 48;
					if ((row <= 10 && arg[3] == '\0') || (row <= 10 && arg[3] == 32))
					{
						if (arg[3] == 32)
						{
							int i = 4;
							while (arg[i] == 32) { i++; }
							if (arg[i] == '\0')
								return 1;
							return 0;
						}
						return 1;
					}
					return 0;
				}
				return 1;
			}
			return 0;
		}
		return 0;
	}
	return 0;
}
int validSetArg(char arg[100])
{
	if (arg[0] >= 'a' && arg[0] <= 'j' || arg[0] >= 'A' && arg[0] <= 'J')
	{
		if (arg[1] >= 48 && arg[1] <= 57)
		{
			int row = arg[1] - 48;
			if ((arg[2] >= 48 && arg[2] <= 57) || arg[2] == 61)
			{
				int i = 2;
				if (arg[2] >= 48 && arg[2] <= 57)
				{
					row = row * 10 + arg[2] - 48;
					i = 3;
				}
				if (row <= 10 && arg[i] == 61)
				{
					i++;
					if ((arg[i] >= 48 && arg[i] <= 57))
					{
						i++;
						while ((arg[i] >= 48 && arg[i] <= 57) && arg[i] != '\0')
						{
							i++;
						}
						if (arg[i] == '\0')
							return 1;
						return 0;
					}
					return 0;
				}
				return 0;
			}
		}
		return 0;
	}
	return 0;
}
int exportValidation(char *arg)
{
	if (arg[0] != '\0')
	{
		int i = 1;
		while (arg[i] != 32 && arg[i] != '\0')
		{
			i++;
		}
		if (arg[i] == '\0')
			return 1;
		else
		{
			while (arg[i] != 32 && arg[i] != '\0')
			{
				i++;
			}
			if (arg[i] == '\0')
				return 1;
			return 0;
		}		
	}
	return 0;
}
int saveValidation(char *arg)
{
	removeSpaces(arg);
	if (arg[0] == '\0')
		return 1;
	return 0;
}


