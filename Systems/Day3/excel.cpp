#include "Excel.h"
void print(int **emptysheet)
{
	emptysheet[0][0] = 32;
	for (int j = 1; j < 11; j++)
	{
		emptysheet[0][j] = 65 + j - 1;
	}
	for (int i = 1; i < 11; i++)
	{
		emptysheet[i][0] = i;
	}
	for (int i = 0; i < 11; i++)
	{
		for (int j = 0; j < 11; j++)
		{
			if (i == 0)
				printf("%6c  |", emptysheet[i][j]);
			else
				printf("%6d  |", emptysheet[i][j]);
		}
		printf("\n");
		for (int i = 1; i < 13; i++)
		{
			printf("%6c%c%c", 45, 45, 45);
		}
		printf("\n");
	}
}
void get(char *arg, int **emptysheet) {
	if (!validGetArg(arg))
	{
		printf("Invalid Format\n");
	}
	else
	{
		int row = arg[1] - 48, col = 0;
		if (arg[0] >= 'a' && arg[0] <= 'z')
		{
			col = arg[0] - 96;
		}
		else if (arg[0] >= 'A' && arg[0] <= 'Z')
		{
			col = arg[0] - 64;
		}
		if (arg[2] != '\0')
		{
			row = row * 10 + arg[2] - 48;
		}
		printf("%d\n", emptysheet[row][col]);
	}
}
void set(char *arg, int **emptysheet) {
	removeSpaces(arg);
	if (!validSetArg(arg))
	{
		printf("Invalid Format\n");
	}
	else
	{
		int row = arg[1] - 48, col = 0;
		if (arg[0] >= 'a' && arg[0] <= 'z')
		{
			col = arg[0] - 96;
		}
		else if (arg[0] >= 'A' && arg[0] <= 'Z')
		{
			col = arg[0] - 64;
		}
		int i = 2;
		if (arg[i] >= 48 && arg[i] <= 57)
		{
			row = row * 10 + arg[i] - 48;
			i = 3;
		}
		int val = 0;
		i++;
		while ((arg[i] >= 48 && arg[i] <= 57) && arg[i] != '\0')
		{
			val = val * 10 + arg[i] - 48;
			i++;
		}
		emptysheet[row][col] = val;
	}
}
char* export1(int **temp_excel, char *argument)
{
	if (!exportValidation(argument))
	{
		printf("Invalid Format\n");
		return '\0';
	}
	char *updatedfilename = (char*)malloc(sizeof(char) * 1000);
	int length = strlen(argument);
	const char *extend = ".csv";
	if (!(argument[length - 1] == extend[3] && argument[length - 2] == extend[2] && argument[length - 3] == extend[1] && argument[length - 4] == extend[0]))
		strcat(argument, extend);
	FILE *fstream=fopen(argument, "w");
	for (int row = 1; row <= 10; row++)
	{
		for (int col = 1; col <= 10; col++)
		{
			if (col == 10)
				fprintf(fstream, "%d\n", temp_excel[row][col]);
			else
				fprintf(fstream, "%d,", temp_excel[row][col]);
		}
	}
	fclose(fstream);
	strcpy(updatedfilename, argument);
	return updatedfilename;
}
char* import(int **temp_excel, char *argument)
{
	char *updatedfilename = (char*)malloc(sizeof(char) * 1000);
	int length = strlen(argument);
	const char *extend = ".csv";
	if (!(argument[length - 1] == extend[3] && argument[length - 2] == extend[2] && argument[length - 3] == extend[1] && argument[length - 4] == extend[0]))
		strcat(argument, extend);
	FILE *fstream;
	fopen_s(&fstream, argument, "r");
	if (fstream == NULL)
	{
		printf("FILE NOT FOUND\n");
		return NULL;
	}
	for (int row = 1; row <= 10; row++)
	{
		for (int col = 1; col <= 10; col++)
		{
			fscanf_s(fstream, "%d,", &temp_excel[row][col]);
		}
	}
	fclose(fstream);
	strcpy(updatedfilename, argument);
	return updatedfilename;
}
void save(int **temp_excel, char *filename)
{
	FILE *fstream = fopen(filename, "w+");
	for (int row = 1; row <= 10; row++)
	{
		for (int col = 1; col <= 10; col++)
		{
			if (col == 10)
				fprintf(fstream, "%d\n", temp_excel[row][col]);
			else
				fprintf(fstream, "%d,", temp_excel[row][col]);
		}
	}
	fclose(fstream);
}

int main()
{
	char input[100];
	char *expFile = (char *)malloc(sizeof(char) * 100); expFile = NULL;
	char *impFile = (char *)malloc(sizeof(char) * 100); impFile = NULL;
	int **emptysheet = (int **)malloc(sizeof(int *) * 12);
	for (int i = 0; i < 12; i++)
	{
		emptysheet[i]= (int *)malloc(sizeof(int) * 12);
	}
	for (int i = 1; i < 11; i++)
	{
		for (int j = 1; j < 11; j++)
		{
			emptysheet[i][j] = 0;
		}
	}
	while (1)
	{
		printf(">");
		gets_s(input);
		while (input[0] == '\0')
		{
			printf("\n>");
			gets_s(input);
		}
		int i=0,k = 0;
		char *command=(char *)malloc(sizeof(char) * 100);
		char *arg=(char *)malloc(sizeof(char) * 100);

		if(input[i] != '\0')
		{
			while (input[i] == 32 || input[i] == 9)
			{
				i++;
			}
			while (input[i] != 32 && input[i] != '\0' && input[i] != 9)
			{
				command[k++] = input[i++];
			}
			command[k] = '\0';
			
			int k = 0;
			arg[k] = '\0';
			for (int j = i; input[i] != '\0';)
			{
				while (input[j] == 32 || input[j] == 9)
				{
					j++;
				}
				while (input[j] != '\0')
				{
					arg[k++] = input[j++];
				}
				arg[k] = '\0';
				break;
			}
		}
		if (compare(command, "get", 3))
			get(arg,emptysheet);
		else if (compare(command, "set", 3))
			set(arg, emptysheet);
		else if (compare(command, "print", 5))
			print(emptysheet);
		else if (compare(command, "export", 6))
		{
			if (!exportValidation(arg))
				printf("Invalid Format\n");
			else
				expFile=export1(emptysheet, arg);
		}
		else if (compare(command, "import", 6))
		{
			if (!exportValidation(arg))
				printf("Invalid Format\n");
			else
				impFile=import(emptysheet, arg);
		}
		else if (compare(command, "save", 4))
		{
			if (saveValidation(arg))
			{
				if(impFile)
					save(emptysheet, impFile);
				else if(expFile)
					save(emptysheet, expFile);
				else
					printf("No File to Save\n");
			}
			else
				printf("Invalid Format\n");
		}
		else
			printf("Invalid Command\n");
	}
	return 0;
}